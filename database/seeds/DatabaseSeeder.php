<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // La creación de datos de roles debe ejecutarse primero
        $this->call(RoleTableSeeder::class);

        // Los usuarios necesitarán los roles previamente generados
        $this->call(UserTableSeeder::class);

        $this->call(CategoryTableSeeder::class);

        $this->call(TagTableSeeder::class);

        $this->call(PostTableSeeder::class);

        $this->call(GradesTableSeeder::class);

        $this->call(SubjectsTableSeeder::class);

        $this->call(GradeSubjectTableSeeder::class);

    }
}
