<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subject = new Subject();
        $subject->name = 'Biología y Geología';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Geografía e Historia';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Lengua Castellana y Literatura';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Matemáticas';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Primera Lengua Extranjera';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Física y Química';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Educación Física';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Valores Éticos';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Biología y Geología';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Matemáticas Orientadas a las Enseñanzas Académicas';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Matemáticas Orientadas a las Enseñanzas Aplicadas';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Cultura Clásica';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Iniciación a la Actividad Emprendedora y Empresarial';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Música';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Tecnología';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Educación Plástica, Visual y Audiovisual';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Segunda Lengua Extranjera';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Religión';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Lengua Cooficial y Literatura';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Economía';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Latín';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Ciencias Aplicadas a la Actividad Profesional';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Artes Escénicas y Danza';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Cultura Científica';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Filosofía';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Tecnologías de la Información y la Comunicación';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Educación Plástica, Visual y Audiovisual';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Sistemas informáticos';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Bases de datos';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Programación';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Inglés tecnico';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Entornos de desarrollo';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Formación y orientación laboral.';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Empresa e iniciativa emprendedora';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Lenguajes de marcas y sistemas de gestión de información';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Desarrollo web en entorno cliente';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Desarrollo web en entorno servidor';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Despliegue de aplicaciones web';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Diseño de interfaces web';
        $subject->save();

        $subject = new Subject();
        $subject->name = 'Proyecto de desarrollo de aplicaciones web';
        $subject->save();

    }
}
