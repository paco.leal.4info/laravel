<?php

use Illuminate\Database\Seeder;
use App\Grade;

class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ID 1
        $grade = new Grade();
        $grade->name = 'FORMACIÓN PROFESIONAL';
        $grade->save();

        //ID 2
        $grade = new Grade();
        $grade->name = 'GRADO MEDIO';
        $grade->parent_id = 1;
        $grade->save();

        //ID 3
        $grade = new Grade();
        $grade->name = 'GRADO SUPERIOR';
        $grade->parent_id = 1;
        $grade->save();

        //ID 4
        $grade = new Grade();
        $grade->name = 'BACHILLER';
        $grade->save();

        //ID 5
        $grade = new Grade();
        $grade->name = 'ESO';
        $grade->save();

        //Grado superior 6
        $grade = new Grade();
        $grade->name = 'DAW';
        $grade->parent_id = 3;
        $grade->save();

        // 7
        $grade = new Grade();
        $grade->name = 'DAM';
        $grade->parent_id = 3;
        $grade->save();

        // 8
        $grade = new Grade();
        $grade->name = 'ASIR';
        $grade->parent_id = 3;
        $grade->save();

        //Grado medio 9
        $grade = new Grade();
        $grade->name = 'Administración y Gestión';
        $grade->parent_id = 2;
        $grade->save();

        // 10
        $grade = new Grade();
        $grade->name = 'Comercio y Márketing';
        $grade->parent_id = 2;
        $grade->save();

        // 11
        $grade = new Grade();
        $grade->name = 'Electricidad y Electrónica';
        $grade->parent_id = 2;
        $grade->save();

        //12
        $grade = new Grade();
        $grade->name = 'Hostelería y Turismo';
        $grade->parent_id = 2;
        $grade->save();

        //13
        $grade = new Grade();
        $grade->name = 'Imagen y Sonido';
        $grade->parent_id = 2;
        $grade->save();

        //Bachiller 14
        $grade = new Grade();
        $grade->name = 'CIENCIAS';
        $grade->parent_id = 4;
        $grade->save();

        // 15
        $grade = new Grade();
        $grade->name = 'Humanidades y Ciencias Sociales';
        $grade->parent_id = 4;
        $grade->save();

        // 16
        $grade = new Grade();
        $grade->name = 'ARTES';
        $grade->parent_id = 4;
        $grade->save();

        //ESO 17
        $grade = new Grade();
        $grade->name = '1º Ciclo';
        $grade->parent_id = 5;
        $grade->save();

        //18
        $grade = new Grade();
        $grade->name = '2º Ciclo';
        $grade->parent_id = 5;
        $grade->save();

        //1 19
        $course = new Grade();
        $course->name = 'Primero';
        $course->parent_id = 17;
        $course->enrollment = '1ESO18';
        $course->save();
        //2 20
        $course = new Grade();
        $course->name = 'Segundo';
        $course->parent_id = 17;
        $course->enrollment = '2ESO18';
        $course->save();
        //3 21
        $course = new Grade();
        $course->name = 'Tercero';
        $course->parent_id = 18;
        $course->enrollment = '3ESO18';
        $course->save();
        //4 22
        $course = new Grade();
        $course->name = 'Cuarto';
        $course->parent_id = 18;
        $course->enrollment = '4ESO18';
        $course->save();
        //5 23
        $course = new Grade();
        $course->name = 'Primero Ciencias';
        $course->parent_id = 14;
        $course->enrollment = '1CBACH18';
        $course->save();
        //6 24
        $course = new Grade();
        $course->name = 'Primero Humanidades';
        $course->parent_id = 15;
        $course->enrollment = '1HBACH18';
        $course->save();
        //7 25
        $course = new Grade();
        $course->name = 'Primero Artes';
        $course->parent_id = 16;
        $course->enrollment = '1ABACH18';
        $course->save();
        //8
        $course = new Grade();
        $course->name = 'Segundo Ciencias';
        $course->parent_id = 14;
        $course->enrollment = '2CBACH18';
        $course->save();
        //9 26
        $course = new Grade();
        $course->name = 'Segundo Humanidades';
        $course->parent_id = 15;
        $course->enrollment = '2HBACH18';
        $course->save();
        //10
        $course = new Grade();
        $course->name = 'Segundo Artes';
        $course->parent_id = 16;
        $course->enrollment = '2ABACH18';
        $course->save();
        //11    27
        $course = new Grade();
        $course->name = 'Primero DAW';
        $course->parent_id = 6;
        $course->enrollment = '1DAW18';
        $course->save();
        //12    28
        $course = new Grade();
        $course->name = 'Segundo DAW';
        $course->parent_id = 6;
        $course->enrollment = '2DAW18';
        $course->save();
    }
}
