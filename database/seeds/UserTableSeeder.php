<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $role_teacher = Role::where('name', 'teacher')->first();
        $role_guest = Role::where('name', 'guest')->first();

        $user = new User();
        $user->name = 'User';
        $user->email = 'user@example.com';
        $user->password = md5('secret');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@example.com';
        $user->password = md5('secret');
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Teacher';
        $user->email = 'teacher@example.com';
        $user->password = md5('secret');
        $user->save();
        $user->roles()->attach($role_teacher);

        $user = new User();
        $user->name = 'Guest';
        $user->email = 'guest@example.com';
        $user->password = md5('secret');
        $user->save();
        $user->roles()->attach($role_guest);

        $user = new User();
        $user->name = 'User';
        $user->email = 'luser@example.com';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'ladmin@example.com';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Teacher';
        $user->email = 'lteacher@example.com';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_teacher);

        $user = new User();
        $user->name = 'Guest';
        $user->email = 'lguest@example.com';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_guest);

        factory(App\User::class, 12)->create();
    }
}
