<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Tag::class, 20)->create();

        $tag = new Tag();
        $tag->name = 'eso';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'ciclos formativos grado medio';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'ciclos formativos grado superior';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'bachillerato ciencias';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'bachillerato humanidades y ciencias sociales';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'bachillerato arte';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Primero';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Segundo';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Tercero';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Cuarto';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Primero Ciencias';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Primero Humanidades';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Primero Artes';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Segundo Ciencias';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Segundo Humanidades';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Segundo Artes';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Primero DAW';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Segundo DAW';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Biología y Geología';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Geografía e Historia';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Lengua Castellana y Literatura';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Matemáticas';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Primera Lengua Extranjera';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Física y Química';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Educación Física';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Valores Éticos';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Matemáticas Orientadas a las Enseñanzas Académicas';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Matemáticas Orientadas a las Enseñanzas Aplicadas';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Cultura Clásica';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Iniciación a la Actividad Emprendedora y Empresarial';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Música';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Tecnología';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Segunda Lengua Extranjera';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Religión';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Lengua Cooficial y Literatura';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Economía';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Latín';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Ciencias Aplicadas a la Actividad Profesional';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Artes Escénicas y Danza';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Cultura Científica';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Filosofía';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Tecnologías de la Información y la Comunicación';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Educación Plástica, Visual y Audiovisual';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Sistemas informáticos';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Bases de datos';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Programación';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Inglés tecnico';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Entornos de desarrollo';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Formación y orientación laboral.';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Empresa e iniciativa emprendedora';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Lenguajes de marcas y sistemas de gestión de información';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Desarrollo web en entorno cliente';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Desarrollo web en entorno servidor';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Despliegue de aplicaciones web';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Diseño de interfaces web';
        $tag->slug = str_slug($tag->name);
        $tag->save();

        $tag = new Tag();
        $tag->name = 'Proyecto de desarrollo de aplicaciones web';
        $tag->slug = str_slug($tag->name);
        $tag->save();
    }
}
