<?php

use Illuminate\Database\Seeder;
use App\Course_subject;

class GradeSubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $relation = new Course_subject();
        $relation->grade_id = 27;
        $relation->subject_id = 28;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 27;
        $relation->subject_id = 29;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 27;
        $relation->subject_id = 30;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 27;
        $relation->subject_id = 31;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 27;
        $relation->subject_id = 32;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 27;
        $relation->subject_id = 33;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 27;
        $relation->subject_id = 34;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 28;
        $relation->subject_id = 35;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 28;
        $relation->subject_id = 36;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 28;
        $relation->subject_id = 37;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 28;
        $relation->subject_id = 38;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 28;
        $relation->subject_id = 39;
        $relation->save();

        $relation = new Course_subject();
        $relation->grade_id = 28;
        $relation->subject_id = 40;
        $relation->save();
    }
}
