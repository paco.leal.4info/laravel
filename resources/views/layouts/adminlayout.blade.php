<?php
use Mini\Core\Session;
use Mini\Core\Functions;
use Mini\Core\Auth;

$downarrow = '&#xf0d7;';
$zone = explode(' ', $title);
$roots = Functions::tree($grades);
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= (isset($title))? $title : 'AcademiaMVC' ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= URL; ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= URL; ?>bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= URL; ?>bower_components/Ionicons/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= URL; ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= URL; ?>dist/css/AdminLTE.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= URL; ?>bower_components/select2/dist/css/select2.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="<?= URL; ?>dist/css/skins/skin-blue.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>MVC</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Academia</b>MVC</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Left menu -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/post" class="">Blog</a></li>
                    <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Courses <span class="caret"></a>
                        <ul class="dropdown-menu" role="menu" style="max-height: 200px; overflow: auto;">
                            <?php if (isset($roots)) {
                                echo Functions::dropdownAdmin($roots);
                            } else { ?>
                                <li class=""><a href="#">There are no courses available</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa fa-bell-o"></i>
                            <span class="label label-info"><?= $unread ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have <?= ($unread)? $unread : 'no new' ?> message<?= ($unread == 0 || $unread > 1)? 's' : '' ?></li>
                            <li>
                                <!-- inner menu: contains the messages -->
                                <ul class="menu">
                                    <?php if ($unread):
                                        $messages = Functions::indexAdmin();
                                        foreach ($messages as $message):?>
                                            <li><!-- start message -->
                                                <a href="/message/show/<?= $message->id ?>">
                                                    <!-- Message title and timestamp -->
                                                    <h4>
                                                        <?= $message->author ?>
                                                        <small><i class="fa fa-clock-o"></i> <?= $message->date ?></small>
                                                    </h4>
                                                    <!-- The message -->
                                                    <p><?= $message->subject ?></p>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <!-- end message -->
                                </ul>
                                <!-- /.menu -->
                            </li>
                            <li class="footer"><a href="/message">See All Messages</a></li>
                        </ul>
                    </li>
                    <!-- /.messages-menu -->

                    <!-- User Account Menu AVATAR-->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="<?= URL; ?>/img/user/<?= $_SESSION ['user']['role'] ?>.png" class="user-image" alt="User Image" <?= ($_SESSION['user']['role'] == 'admin')? 'style="background: rgba(250,250,250,0.7);"' : '' ?>>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?= $_SESSION ['user']['name'] ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="<?= URL; ?>/img/user/<?= $_SESSION ['user']['role'] ?>.png" class="img-circle" alt="User Image" <?= ($_SESSION['user']['role'] == 'admin')? 'style="background: rgba(250,250,250,0.7);"' : '' ?>>
                                <p><?= $_SESSION ['user']['name'] ?></p>
                                <small style="color:white;"><?= $_SESSION ['user']['role'] ?></small>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="/teacher/newPost" class="btn btn-default btn-flat pull-left" style="font-weight: bolder;">+ <span class="fa fa-file"></span></a>
                                    <a href="/message/new" class="btn btn-default btn-flat pull-left" style="font-weight: bolder;">+ <span class="fa fa-envelope"></span></a>
                                </div>
                                <div class="pull-right">
                                    <a href="/login/logout" class="btn btn-default btn-flat pull-right"><span class="fa fa-sign-out"></span> Log out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?= URL; ?>/img/user/<?= $_SESSION['user']['role'] ?>.png" class="img-circle" alt="User Image" <?= ($_SESSION['user']['role'] == 'admin')? 'style="background: rgba(250,250,250,0.7);"' : '' ?>>
                </div>
                <div class="pull-left info">
                    <p><?= $_SESSION['user']['name'] ?></p>
                    <small><?= $_SESSION['user']['role'] ?></small>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <!-- Optionally, you can add icons to the links -->
                <li <?= (in_array('Post', $zone))? 'class="active"' : '' ?>><a href="/teacher/posts"><i class="fa fa-link"></i> <span>Posts</span></a></li>
                <li <?= (in_array('Grades', $zone))? 'class="active"' : '' ?>><a href="/teacher/grades"><i class="fa fa-link"></i> <span>Grades/Courses</span></a></li>
                <?php if (Auth::checkAuth('admin', false)):?>
                <li <?= (in_array('Category', $zone))? 'class="active"' : '' ?>><a href="/admin/categories"><i class="fa fa-link"></i> <span>Categories</span></a></li>
                <li <?= (in_array('Tag', $zone))? 'class="active"' : '' ?>><a href="/admin/tags"><i class="fa fa-link"></i> <span>Tags</span></a></li>
                <?php endif; ?>
                <li <?= (in_array('User', $zone))? 'class="active"' : '' ?>><a href="/user"><i class="fa fa-link"></i> <span>Users</span></a></li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content container-fluid">
            <?= $this->insert('admin/partials/message') ?>
            <?= $this->section('content') ?>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Anything you want, you've got it
            <a href="#" id="back" class="adminfloat">
                <i class="fa fa-arrow-left" style="font-size: 120%;margin-top:0.55em;"></i>
            </a>
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2018 <a href="http://www.gitlab.com/paco.leal.4info">FºJosé Leal</a>.</strong> All rights reserved.
    </footer>

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?= URL; ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= URL; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?= URL; ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- DataTables -->
<script src='<?= URL ?>bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
<script src='<?= URL ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>
<!-- SlimScroll -->
<script src="<?= URL; ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= URL; ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= URL; ?>dist/js/adminlte.min.js"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
<!-- CKEditor -->
<script src="<?= URL; ?>bower_components/ckeditor/ckeditor.js"></script>
<script>
    $(document).ready(function () {
        $('#back').click(function() {
            parent.history.back();
            return false;
        });

        $('#example1').DataTable({
            'columnDefs': [
                { 'orderable': false, 'targets': 'no-sort' }
            ]
        });
        $('.select2').select2();
    });

    CKEDITOR.replace('body');
    CKEDITOR.config.height = '295px';
</script>
</body>
</html>