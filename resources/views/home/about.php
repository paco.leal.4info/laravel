<?= $this->layout('layouts/publiclayout'); ?>
<section class="pages container">
    <div class="page page-about">
        <h1 class="text-capitalize"><?= $title ?></h1>
        <cite>This project was developed by <a href="https://www.gitlab.com/paco.leal.4info">FºJosé Leal Ballesta</a> from <a href="http://moodle.iescierva.net">IES Ingeniero de la Cierva</a>, Spain (Murcia)</cite>
        <div class="divider-2" style="margin: 35px 0;"></div>
        <p>Both Zendero and <a href="https://adminlte.io/">AdminLTE</a> are free to use templates, provided by teachers or the Internet. Althought they where modified for this project, their rights go to the respective owners, Agencia de la Web and <a href="https://adminlte.io/about">Abdullah Almsaeed</a>, respectively.</p>
        <p>Francisco José Leal Ballesta is a Web Development student in IES Ingeniero de la Cierva in Spain (Murcia). He is passionate about web development and eager to learn more and create great web application. To reach him, you may follow him on <a href="https://www.twitter.com/FcoJoseLeal">Twitter</a>. </p>
    </div>
</section>