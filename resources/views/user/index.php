<?= $this->layout('layouts/adminlayout');
use \Mini\Core\Auth;
?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <?php if (($_SESSION['user']['role'] == 'teacher') && isset($_SESSION['user']['grades'][0])): ?>
                    <form action="/home/unroll/<?= $_SESSION['user']['grades'][0] ?>">
                        <h3 class="box-title"><?= $title ?></h3><?= ($_SESSION['user']['role'] == 'teacher')? '<button type="submit" class="btn btn-danger pull-right" onclick="return confirm(\'Are you sure?\')">Unroll Me</button>' : '' ?>
                    </form>
                    <?php else: ?>
                        <h3 class="box-title"><?= $title ?>
                    <?php endif; ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                        <?php if (isset($users) && !empty($users)): ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="width: 5%;">ID</th>
                                <th>User <i class="fa fa-pencil"></th>
                                <th>Email <i class="fa fa-pencil"></th>
                                <th>Role</th>
                                <th style="width: 10%;" class="no-sort">Update</th>
                                <th style="width: 10%;" class="no-sort">Delete</th>
                                <th style="width: 10%;" class="no-sort">Send Message</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($users as $user): ?>
                                <tr>
                                    <td><?= $user->id ?></td>
                                    <form action="/user/update/<?= $user->id ?>" method="POST">
                                        <td><input type="text" name="name" style="border:0; background-color: transparent;" value="<?= $user->name ?>"><span hidden><?= $user->name ?></span></td>
                                        <td><input type="text" name="email" style="border:0; width:100%; background-color: transparent;" value="<?= $user->email ?>"><span hidden><?= $user->email ?></span></td>
                                        <td><select name="role_id" style="border:0; width:100%; background-color: transparent;">
                                                <?php foreach ($roles as $role): ?>
                                                    <option value="<?= $role->id ?>" <?= ($role->name == $user->role)? 'selected' : '' ?>><?= $role->name ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <span hidden><?= $user->role ?></span>
                                        </td>
                                        <td><button class="btn btn-sm btn-info btn-block"><i class="fa fa-floppy-o"></i> Save</button></td>
                                    </form>
                                    <td>
                                    <?php if (Auth::checkAuth('admin', false)): ?>
                                        <a href="/user/delete/<?= $user->id ?>" class="btn btn-sm btn-danger btn-block" onClick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</span></a>
                                    <?php else: ?>
                                        <p class="text-center" style="color:grey; font-weight: bold; font-size: 1.5em; margin:0; padding:0;"><i class="fa fa-ban"></i></span></p>
                                    <?php endif; ?>
                                    </td>
                                    <td><a href="/message/new/<?= $user->id ?>" class="btn btn-sm btn-default btn-block"><i class="fa fa-envelope"></i> Message</span></a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else:
                            if (($_SESSION['user']['role'] == 'teacher') && isset($_SESSION['user']['grades'][0])): ?>
                                <h2 class="text-secondary">There are no Users yet</h2>
                            <?php elseif (($_SESSION['user']['role'] == 'admin')): ?>
                                <h2 class="text-secondary">There are no Users yet</h2>
                            <?php else: ?>
                                <h2 class="text-secondary">You have to enroll in a Course to see your student list</h2>
                    <?php   endif;
                    endif; ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>