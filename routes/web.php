<?php
use App\Grade;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/about', 'HomeController@about')->name('about');

Route::get('/blog', 'BlogController@index')->name('blog');
Route::resource('post', 'PostController');
Route::resource('admin', 'AdminController');
Route::resource('category', 'CategoryController');
Route::resource('grade', 'GradeController');
Route::resource('message', 'MessageController');
Route::resource('tag', 'TagController');
Route::resource('teacher', 'TeacherController');

Route::get('/pruebas', function() {
    $grades = new Grade();
    $grades = $grades->whereNull('parent_id')->get();
    /*$grades = $grades->first();
    dd($grades->grades);*/

    tree($grades);


    /*
    foreach ($grades as $grade) {
        echo $grade->name . ' => ';
        foreach ($grade->grades as $son) {
            echo $son->name . ', ';
            foreach ($son->grades as $son2) {
                echo $son2->name . ', ';
                var_dump($son2->grades);
                if (count($son2->grades) < 1) {
                    echo "<br>HOLA<br>";
                }
                dd($son2);
                echo '<br><br>';/*
                foreach ($son2->grades as $son3) {
                    echo $son3->name . ', ';
                    //dd($son3->grades);
                    if (!empty($son3->grades)) {
                        echo 'HOLA';
                    }
                }
            }
        }
        echo '<br>';
    }*/


});

function tree($grades)
{
    foreach ($grades as $grade) {
        echo $grade->name . '<br>';
        if (count($grade->grades) > 0) {
            tree($grade->grades);
        }
    }
}