<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $fillable = [
        'name',
    ];

    public function parent()
    {
        return $this->belognsTo('\App\Grade');
    }

    public function grades()
    {
        return $this->hasMany('\App\Grade', 'parent_id');
    }
}
