<?php

namespace App;

class Functions
{


    public static function publicdropdown($grades=null, $dropdown='')
    {
        $grades = new Grade();
        $grades = $grades->whereNull('parent_id')->get();
        $sidearrow = '&#xf0da;';
        foreach ($grades as $grade) {
            $dropdown .= "<li><a tabindex=\"-1\" href=\"/grade/index/$grade->id\" class=\"text-uppercase\">$grade->name";
            if (count($grade->grades) > 0) {
                $dropdown .= "<i class=\"fa\" style=\"font-size:18px; margin-left: 0.2em;\">$sidearrow</i>";
                self::publicdropdown($grade->grades, $dropdown);
            }
            $dropdown .= "</a></li>";
        }

        return $dropdown;
    }

    public static function getGrades()
    {
        $grades = new Grade();
        $grades = $grades->whereNull('parent_id')->get();
        $dropdown = self::tree($grades);
        return $dropdown;
    }

    public static function tree($grades, $dropdown='')
    {
        $sidearrow = '&#xf0da;';
        foreach ($grades as $grade) {
            $dropdown .= "<li><a tabindex=\"-1\" href=\"/grade/index/$grade->id\" class=\"text-uppercase\">$grade->name";
            if (count($grade->grades) > 0) {
                $dropdown .= "<i class=\"fa\" style=\"font-size:18px; margin-left: 0.2em;\">$sidearrow</i>";
                self::tree($grade->grades, $dropdown);
            }
            $dropdown .= "</a></li>";
        }
        return $dropdown;
    }
}